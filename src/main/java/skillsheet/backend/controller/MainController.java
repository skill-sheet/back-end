package skillsheet.backend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MainController {

    @GetMapping({"/","/home"})
    public String home(){
        return "home";
    }

    @GetMapping("/test")
    public String test(){
        return "This is test message";
    }

}
