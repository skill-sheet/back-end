package skillsheet.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendApplication {

    public static void main(String[] args) {
	//init project
        SpringApplication.run(BackendApplication.class, args);
        System.out.println("Test");
    }

}
